﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;


    private bool can_Shoot = true;

    // Update is called once per frame
    void Update()
    {
        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space) && can_Shoot)
        {
            Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
            can_Shoot = false;

            StartCoroutine(ResetAbility());
        }
    }


    private IEnumerator ResetAbility()
    {

        yield return new WaitForSeconds(1);
        can_Shoot = true;
    }



}
