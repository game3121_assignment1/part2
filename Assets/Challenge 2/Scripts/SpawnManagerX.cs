﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    public GameObject[] ballPrefabs;

    private float spawnLimitXLeft = -22;
    private float spawnLimitXRight = 7;
    private float spawnPosY = 30;

    private float startDelay = 1.0f;
    private float spawnInterval = 3.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitAndSpawn(spawnInterval));
    }

    // Spawn random ball at random x position at top of play area
    void SpawnRandomBall ()
    {
        //Generate random seed so numbers arent repeated.
        Random.seed = System.DateTime.Now.Millisecond;

        spawnInterval = Random.Range(3.0f, 5.0f);
        // Generate random ball index and random spawn position
        Vector3 spawnPos = new Vector3(Random.Range(spawnLimitXLeft, spawnLimitXRight), spawnPosY, 0);
        
        // Choose which ball prefab to spawn.
        int ballToSpawn = Random.Range(0, 3);
        // instantiate ball at random spawn location
        Instantiate(ballPrefabs[ballToSpawn], spawnPos, ballPrefabs[ballToSpawn].transform.rotation);
        //Start Coroutine
        StartCoroutine(WaitAndSpawn(spawnInterval));
    }
    private IEnumerator WaitAndSpawn(float waitTime)
    {
        //wait n seconds before restarting
            yield return new WaitForSeconds(waitTime);
            //spawn ball
            SpawnRandomBall();

    }
    

}
